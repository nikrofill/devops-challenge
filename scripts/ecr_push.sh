cd ../backend
docker build -t backend:latest .
docker tag backend:latest 171780636263.dkr.ecr.us-west-2.amazonaws.com/test:backend
docker push 171780636263.dkr.ecr.us-west-2.amazonaws.com/test:backend
cd ../frontend
docker build -t frontend:latest .
docker tag frontend:latest 171780636263.dkr.ecr.us-west-2.amazonaws.com/test:frontend
docker push 171780636263.dkr.ecr.us-west-2.amazonaws.com/test:frontend