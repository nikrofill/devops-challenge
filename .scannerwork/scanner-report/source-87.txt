apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{  .Release.Name  }}-deployment
  labels:
    name: {{  .Release.Name  }}-deployment
    cluster: prod 
spec:
  replicas: {{ .Values.replicas_count  }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
  selector:
    matchLabels:
      cluster: prod 
  template:
    metadata:
      labels:
        cluster: prod 
    spec:
      containers:
      - name: backend
        image: {{ .Values.image }}
        resources:
          limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
        - containerPort: 3000
