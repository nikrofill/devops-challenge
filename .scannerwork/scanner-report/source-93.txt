apiVersion: v1 
kind: Service 
metadata:
  name: {{  .Release.Name  }}
  labels:
    app: {{  .Release.Name  }}
    cluster: prod 
spec:
  selector: 
    app: {{  .Release.Name  }}
    cluster: prod 
  ports:
    - name: {{  .Release.Name  }}-listener
      protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer