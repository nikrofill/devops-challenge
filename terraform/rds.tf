resource "aws_db_subnet_group" "default" {
  name       = "app"
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name = "app"
  }
}

resource "aws_db_instance" "app" {
  allocated_storage    = 10
  db_name              = "app"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = var.db_username
  password             = var.db_password
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.internal.id]
  db_subnet_group_name = aws_db_subnet_group.default.name
}